#!/usr/bin/env python
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH -J KfoldsH2O_rbf_G12
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=8G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=muammar@brown.edu

from amp import Amp
from amp.descriptor.botu import Botu, make_symmetry_functions
from amp.model.kernel_ridge import KRR

from ase.io.trajectory import Trajectory
import numpy as np
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error
import subprocess

import os.path


# Input
lamdas = np.logspace(-40., 1., num=10, endpoint=True, base=2.0, dtype=None)
lamdas = [9.09494701773e-13]
sigmas = np.logspace(-5., 5., num=40, endpoint=True, base=2.0, dtype=None)

images = Trajectory('trainingimages.traj')
group_size = 4

lenght = range(len(images))
list_of_groups = zip(*(iter(lenght),) * group_size)

kernel = 'rbf'
G = None


if os.path.isfile('grid_search.txt'):
    f = open('grid_search.txt', 'a')
else:
    f = open('grid_search.txt', 'w')
    #f.write('%s %8s %8s %8s %8s %8s %8s %8s\n' % ('Lamda', 'Sigma', 'MAE (E)', 'RMSE (E)', 'R2 (E)', 'MAE (F)', 'RMSE (F)', 'R2 (F)'))
    f.write('%s %8s %8s %8s %8s %8s\n' % ('Lamda', 'Sigma', 'MAE (E)', 'RMSE (E)', 'MAE (F)', 'RMSE (F)', ))

def loss(lamda, sigma):
    """docstring for loss"""

    lamda = lamda
    sigma = sigma

    for test_set in list_of_groups:
        mae_energy = []
        rmse_energy = []
        r2_energy = []
        mae_forces = []
        rmse_forces = []
        r2_forces = []

        training = set(lenght) - set(test_set)
        ti_label = 'calc-l{}-s{}-set_{}_{}.traj' .format(str(lamda), str(sigma), str(test_set[0]), str(test_set[-1]))
        ti = Trajectory(ti_label , mode='w')
        #trainingimages = [images[i] for i in training]

        trainingimages = []

        for i in training:
            trainingimages.append(images[i])
            ti.write(images[i])

        test = [images[i] for i in test_set]

        logfile =  'calc-l{}-s{}-set_{}_{}-log.txt' .format(str(lamda), str(sigma), str(test_set[0]), str(test_set[-1]))

        if not os.path.isfile(logfile):
            try:
                label = 'calc-l{}-s{}-set_{}_{}' .format(str(lamda), str(sigma), str(test_set[0]), str(test_set[-1]))

                ##elements = ['H', 'O']
                ##G = make_symmetry_functions(elements=elements, type='G2',
                ##            etas=np.logspace(np.log10(0.05), np.log10(80.),
                ##                             num=6))
                ###G += make_symmetry_functions(elements=elements, type='G4',
                ###             etas=[0.005],
                ###             zetas=[1.],#, 4.],
                ###             gammas=[+1., -1.])

                ##G = {'H': G,
                ##   'O': G}
                calc = Amp(
                        descriptor=Botu(cutoff=6.5, Gs=G),
                        model=KRR(kernel=kernel, lamda=lamda, sigma=sigma,
                            trainingimages=ti_label, cholesky=True, forcetraining=True),
                        label=label,
                        cores=1
                        )
                calc.train(images=trainingimages)

                del calc
                calc1 = Amp.load(label + '.amp')
                calc1.set_label('amp-l' + str(lamda) + '-s' + str(sigma))

                true_energies = []
                amp_energies = []
                true_forces = []
                amp_forces = []

                for image in test:
                    true = image.get_potential_energy(apply_constraint=False)
                    amp = calc1.get_potential_energy(image)
                    true_f = np.ravel(image.get_forces(apply_constraint=False))
                    amp_f = np.ravel(calc1.get_forces(image))

                    true_energies.append(true)
                    amp_energies.append(amp)
                    true_forces.append(true_f)
                    amp_forces.append(amp_f)

                mae_energy.append(mean_absolute_error(true_energies, amp_energies))
                print('Sigma = {}' .format(sigma))
                print(mae_energy)
                rmse_energy.append(np.sqrt(mean_squared_error(true_energies, amp_energies)))
                #r2_energy.append(r2_score(true_energies, amp_energies))

                mae_forces.append(mean_absolute_error(true_forces, amp_forces))
                print(mae_forces)
                rmse_forces.append(np.sqrt(mean_squared_error(true_forces, amp_forces)))
                #r2_forces.append(r2_score(true_forces, amp_forces))

                remove = ['rm', '-R', 'amp-fingerprints.ampdb', 'amp-neighborlists.ampdb', 'amp-log.txt']
                subprocess.call(remove)
            except KeyboardInterrupt:
                raise
            except:
                pass

    try:
        mae = np.mean(mae_energy)
        rmse = np.mean(rmse_energy)
        #r2 = np.mean(r2_energy)
        mae_f = np.mean(mae_forces)
        rmse_f = np.mean(rmse_forces)
        print('Force_MAE = {}' .format(mae_f))
        #r2_f = np.mean(r2_forces)
        #f.write('%.4e %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n' % (lamda, sigma, mae, rmse, r2, mae_f, rmse_f, r2_f))
        f.write('%.4e %8.4f %8.4f %8.4f %8.4f %8.4f \n' % (lamda, sigma, mae, rmse, mae_f, rmse_f))
        f.flush()
    except:
        msg = 'NC'
        f.write('%.4e %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n' % (lamda, sigma, msg, msg, msg, msg, msg, msg))
        f.flush()


for lamda in lamdas:
    for sigma in sigmas:
        loss(lamda, sigma)
