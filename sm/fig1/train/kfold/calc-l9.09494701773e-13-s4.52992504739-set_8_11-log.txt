
   oo      o       o   oooooo
  o  o     oo     oo   o     o
 o    o    o o   o o   o     o
o      o   o  o o  o   o     o
oooooooo   o   o   o   oooooo
o      o   o       o   o
o      o   o       o   o
o      o   o       o   o

Amp: Atomistic Machine-learning Package
Developed by Andrew Peterson, Alireza Khorshidi, and others,
Brown University.
PI Website: http://brown.edu/go/catalyst
Official repository: http://bitbucket.org/andrewpeterson/amp
Official documentation: http://amp.readthedocs.io/
Citation:
  Alireza Khorshidi & Andrew A. Peterson,
  Computer Physics Communications 207: 310-324 (2016).
  http://doi.org/10.1016/j.cpc.2016.05.010
======================================================================
User: muammar
Hostname: nuc
Date: 2018-01-23T20:56:49 (2018-01-24T01:56:49 UTC)
Architecture: x86_64
PID: 22702
Amp version: (development)
Amp directory: /home/muammar/brown/git/amp/amp
 Last commit: 0b77b591b042271e3ac4aba7f1cc9510ee7ab881
 Last commit date: 2018-01-23 16:38:17 -0500
Python: v2.7.14: /usr/bin/python
ASE v3.15.1b1: /home/muammar/brown/git/ase/ase
NumPy v1.13.3: /home/muammar/.local/lib/python2.7/site-packages/numpy
SciPy v0.18.1: /home/muammar/.local/lib/python2.7/site-packages/scipy
ZMQ/PyZMQ v4.1.6/v16.0.2: /home/muammar/.local/lib/python2.7/site-packages/zmq
pxssh: Not available from pxssh.
pxssh (via pexpect v4.2.1): /home/muammar/.local/lib/python2.7/site-packages/pexpect/pxssh.pyc
======================================================================
Serial operation on one core specified.

Amp training started. 2018-01-23T20:56:49

Descriptor: Botu
  (<amp.descriptor.botu.Botu object at 0x7f7371e748d0>)
Model: KRR
  (<amp.model.kernel_ridge.KRR object at 0x7f7371e74750>)
Hashing images...
 12 unique images after hashing.
...hashing completed. 0.0 min.

Descriptor
==========
Cutoff function: <Cosine cutoff with Rc=6.500 from amp.descriptor.cutoffs>
Finding unique set of elements in training data.
2 unique elements included: H, O
No symmetry functions supplied; creating defaults.
Number of symmetry functions for each element:
  H: 8
  O: 8
H feature vector functions:
 0: G2, H, eta = 0.05
 1: G2, O, eta = 0.05
 2: G2, H, eta = 4.0
 3: G2, O, eta = 4.0
 4: G2, H, eta = 20.0
 5: G2, O, eta = 20.0
 6: G2, H, eta = 80.0
 7: G2, O, eta = 80.0
O feature vector functions:
 0: G2, H, eta = 0.05
 1: G2, O, eta = 0.05
 2: G2, H, eta = 4.0
 3: G2, O, eta = 4.0
 4: G2, H, eta = 20.0
 5: G2, O, eta = 20.0
 6: G2, H, eta = 80.0
 7: G2, O, eta = 80.0
Calculating neighborlists...
 Data stored in file calc-l9.09494701773e-13-s4.52992504739-set_8_11-neighborlists.
 File exists with 0 total images, 0 of which are needed.
 12 new calculations needed.
 Calculated 12 new images.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file calc-l9.09494701773e-13-s4.52992504739-set_8_11-fingerprints.
 File exists with 0 total images, 0 of which are needed.
 12 new calculations needed.
 Calculated 12 new images.
...fingerprints calculated. 0.0 min.
Calculating fingerprint derivatives...
 Data stored in file calc-l9.09494701773e-13-s4.52992504739-set_8_11-fingerprint-primes.
 File exists with 0 total images, 0 of which are needed.
 12 new calculations needed.
 Calculated 12 new images.
...fingerprint derivatives calculated. 0.0 min.

Model fitting
=============
Regression in atom-centered mode.
Calculating rbf kernel...
Parameters:
 lamda = 9.09494701773e-13
 sigma = 4.52992504739
...kernel computed in 0.0 min.
Initializing weights.
Starting Cholesky decomposition of kernel energy matrix to get upper triangular matrix.
... Cholesky Decomposing finished in. 0.0 min.
Starting Cholesky decomposition of kernel force matrix to get upper triangular matrix.
... Cholesky Decomposing finished in. 0.0 min.
Amp successfully trained. Saving current parameters.
Parameters saved in file "calc-l9.09494701773e-13-s4.52992504739-set_8_11.amp".
This file can be opened with `calc = Amp.load('calc-l9.09494701773e-13-s4.52992504739-set_8_11.amp')`
