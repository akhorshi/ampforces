from amp import Amp
from amp.model.kernel_ridge import KRR
from amp.descriptor.botu import Botu

from ase.io import Trajectory

lamda = 9.0949e-13
sigma = 0.6413
images = 'trainingimages.traj'
label = 'kfold'
kernel = 'rbf'
G = None

calc = Amp(
        descriptor=Botu(fortran=False, Gs=G),
        model=KRR(kernel=kernel, lamda=lamda, sigma=sigma,
                  trainingimages=images, cholesky=True, forcetraining=True),
        label=label,
        cores=1
        )

trainingimages = Trajectory(images)
calc.train(images=trainingimages)
