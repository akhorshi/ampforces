#!/usr/bin/env python
# General Python import
import numpy as np
import numpy.linalg as la
import math
import os

# ASE imports
from ase import Atoms
from ase.io import Trajectory, read
from ase.calculators.nwchem import NWChem

# Plot
import matplotlib.pyplot as plt


# Needed methods to prepare the structures
def py_ang(v1, v2):
    """Method to define the angle between two vectors"""
    cosang = np.dot(v1, v2)
    sinang = la.norm(np.cross(v1, v2))
    return np.arctan2(sinang, cosang)


def vector(lenght, angle):
    """vector direction control"""
    angle = ((angle) * np.pi) / 360
    v1 = [(math.sin(angle) * lenght), (math.cos(angle) * lenght), 0]
    v2 = [-(math.sin(angle) * lenght), (math.cos(angle) * lenght), 0]
    return v1, v2


"""
Generation of atom objects and computation of images
"""
traj = Trajectory('images.traj', 'w')
distance = 0.969
angles = range(30, 190, 10)

forces = []
rads = []
for angle in angles:
    print('Angle = {}' .format(angle))
    v = vector(distance, angle)
    print(py_ang(v[0], v[1]))
    rads.append(py_ang(v[0], v[1]))
    atoms = Atoms(numbers=[1, 8, 1],
                  positions=[v[0], [0, 0, 0], v[1]],)

    calc = NWChem(label='calc/nwchem',
                  maxiter=200,
                  xc='B3LYP',
                  basis='6-31+G*',)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()

    append = atoms.get_forces(apply_constraint=False)
    print(append[0])
    forces.append(append)

    traj.write(atoms)

    print('Images computed and written in images.traj')
