#!/usr/bin/env python
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH -J generate_water
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=8G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=muammar@brown.edu

from ase.io import read, Trajectory
from ase.visualize import view
from ase.calculators.nwchem import NWChem

from amp import Amp
import subprocess

traj = Trajectory('rotation.traj')

calc = Amp.load('kfold.amp')
for iteration, atoms in enumerate(traj):
    print('Iteration: {}' .format(iteration))
    #print('Energy DFT: {}' .format(atoms.get_potential_energy()))
    #print('Energy Amp: {}' .format(calc.get_potential_energy(atoms)))
    print('Forces DFT: {}' .format(atoms.get_forces()))
    amp_forces = calc.get_forces(atoms)
    print('Net Forces Amp')
    for atom in amp_forces:
        print(atom)
    subprocess.call(['rm', '-r', 'amp-fingerprints.ampdb',  'amp-log.txt',
        'amp-neighborlists.ampdb', 'amp-fingerprint-primes.ampdb'])
