#!/usr/bin/env python
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH -J generate_water_rotation
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=8G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=muammar@brown.edu

from ase.io import read, Trajectory
from ase.visualize import view
from ase.calculators.nwchem import NWChem


angles = range(0, 185, 5)

traj = Trajectory('rotation.traj', 'w')

for angle in angles:
    atoms = read('../water_bending/images.traj', index=8)
    atoms.rotate(angle, 'z')
    calc = NWChem(label='calc/nwchem',
                  maxiter=200,
                  xc='B3LYP',
                  basis='6-31+G*',)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    atoms.get_forces(apply_constraint=False)
    traj.write(atoms)
