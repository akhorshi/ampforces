#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ase import Atoms
from ase.io import Trajectory
from ase.calculators.nwchem import NWChem
import numpy as np

req = 0.969
distances = np.arange(req, 5, .5)

images = Trajectory('stretch.traj', mode='w')

for distance in distances:
    atoms = Atoms(numbers=[1, 8, 1],
                  positions=[[distance, 0, 0], [0, 0, 0], [0, req, 0]])

    calc = NWChem(label='calc/nwchem',
                  maxiter=200,
                  xc='B3LYP',
                  basis='6-31+G*',)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    atoms.get_forces(apply_constraint=False)
    images.write(atoms)

images.close()
