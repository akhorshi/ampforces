#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ase.build import molecule
from ase.optimize import BFGS
from ase.calculators.nwchem import NWChem

water = molecule('H2O')

calc = NWChem(label='calc/nwchem',
              maxiter=200,
              xc='B3LYP',
              basis='6-31+G*',
              )

water.set_calculator(calc)

dyn = BFGS(water, trajectory='water_optimization.traj')
dyn.run(fmax=0.01)
