#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv

f = open('results.txt', 'r')
data = csv.reader(f, dialect="excel-tab")
angles = []
dft = []
krr =[]
for line in data:
    angles.append(line[0])
    dft.append(line[1])
    krr.append(line[2])


fig, ax1 = plt.subplots()
ax1.plot(angles, dft, 'b-')
ax1.set_xlabel('angles (s)')
# Make the y-axis label, ticks and tick labels match the line color.
ax1.set_ylabel('DFT Force', color='b')
ax1.tick_params('y', colors='b')

ax2 = ax1.twinx()
ax2.plot(angles, krr, 'r.')
ax2.set_ylabel('force', color='r')
ax2.tick_params('y', colors='r')
fig.tight_layout()
plt.show()
