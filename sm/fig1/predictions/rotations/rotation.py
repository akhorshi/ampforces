#!/usr/bin/env python
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH -J generate_water
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=8G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=muammar@brown.edu

from ase.io import read, Trajectory
from ase.visualize import view
from ase.calculators.nwchem import NWChem

from amp import Amp
import subprocess

traj = Trajectory('rotation.traj')
calc = Amp.load('kfold.amp')
angles = range(0, 365, 5)

log = open('results.txt', 'w')
log.write('#angle   #dft    #krr\n')
for iteration, atoms in enumerate(traj):
    #print('Energy DFT: {}' .format(atoms.get_potential_energy()))
    #print('Energy Amp: {}' .format(calc.get_potential_energy(atoms)))
    amp_forces = calc.get_forces(atoms)
    log.write(str(angles[iteration]) + '\t' + str(atoms.get_forces()[0][0]) +
              '\t' + str(amp_forces[0][0]) + '\n')
    #print('Net Forces Amp')
    #for atom in amp_forces:
    #    print(atom)
    subprocess.call(['rm', '-r', 'amp-fingerprints.ampdb',  'amp-log.txt',
                     'amp-neighborlists.ampdb', 'amp-fingerprint-primes.ampdb'])

log.close()
