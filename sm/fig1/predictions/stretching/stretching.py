#!/usr/bin/env python
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH -J generate_water
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=8G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=muammar@brown.edu

from ase.io import read, Trajectory
from ase.visualize import view
from ase.calculators.nwchem import NWChem

from amp import Amp

import subprocess

traj = Trajectory('stretch.traj')
calc = Amp.load('kfold.amp')
req = 0.969

log = open('results.txt', 'w')
log.write('#distance   #dft    #krr\n')
for iteration, atoms in enumerate(traj):
    amp_forces = calc.get_forces(atoms)
    distance = atoms.get_distance(0, 1)
    log.write(str(distance) + '\t' + str(atoms.get_forces()[1][1]) +
              '\t' + str(amp_forces[1][1]) + '\n')
    subprocess.call(['rm', '-r', 'amp-fingerprints.ampdb',  'amp-log.txt',
                     'amp-neighborlists.ampdb', 'amp-fingerprint-primes.ampdb'])

log.close()
