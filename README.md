# Structure

- localvector: code with the fingerprint from Li et al.
- sm: supplementary material.
- krr: snapshot used for computing force magnitudes. You need to copy the
  `localvector.py` module in `amp/descriptor`

  ```
  cp localvector.py krr/amp/descriptor/
  ```
